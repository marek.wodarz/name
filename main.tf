resource "random_id" "appendix" {
  byte_length = 2
}

variable "prefix" {
  type = string
}

variable "environment" {
  type = string
}

variable "name" {
  type = string
}

output "name" {
  value = "${var.prefix}-${var.environment}-${var.name}-${random_id.appendix.hex}"
}
